# Contributions de partenaires au projet Textométrie / TXM

* corpus exemples
* scripts
* feuilles XSL
* css
* documentation
* support de cours
* etc.

Voir les répertoires respectifs ci-dessus.
