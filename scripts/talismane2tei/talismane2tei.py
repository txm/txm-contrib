# Bruno Oberle
# v1.0.0 2017-10-24
# v1.1.0 2017-10-24: adding support for TreeTagger

import argparse, collections, re, os, sys
import talismaneloc2loc, tt2ttloc
from xml.sax.saxutils import escape, quoteattr
from bs4 import BeautifulSoup

ConllXToken = collections.namedtuple('ConllXToken',
    'ID FORM LEMMA CPOSTAG POSTAG FEATS HEAD DEPREL PHEAD PDEPREL ' + \
    'startpos endpos')

GlozzParagraph = collections.namedtuple('GlozzParagraph',
    'start end counter')

#GlozzAnnotation = collections.namedtuple('GlozzAnnotation',
#    'start end refname')

class GlozzAnnotation:

    counter = 1

    def __init__(self, start, end, refname):
        self.start = start
        self.end = end
        self.refname = refname
        self.start_is_used = False
        self.end_is_used = False
        self.counter = GlozzAnnotation.counter
        GlozzAnnotation.counter += 1

    def __str__(self):
        return "%s: %d-%d (%d)" % (self.refname, self.start, self.end,
            self.counter)

class TokenCollection:

    def __init__(self):
        self.tokens = []

    def add(self, token):
        self.tokens.append(token)

    def inject_into_corpus(self, corpus):
        res = ""
        last_pos = 0
        for token in self.tokens:
            #if last_pos != token.startpos:
            res += escape(corpus[last_pos:token.startpos])
            res += token.make_tags(corpus[token.startpos:token.endpos])
            last_pos = token.endpos
        res += corpus[last_pos:]
        return res

    def inject_glozz_annotations(self, glozz_annots):
        # any annotation beyond the last token?
        border = self.tokens[-1].endpos
        for annot in glozz_annots:
            if annot.end > border:
                annot.end = border
        for token in self.tokens:
            token.absorb_glozz(glozz_annots)
        for annot in glozz_annots:
            assert annot.start_is_used, str(annot)
            assert annot.end_is_used, str(annot)

    def print_positions(self):
        for token in self.tokens:
            print("%d -- %d-%d -- %d" % (token.outer_startpos, token.startpos,
                token.endpos, token.outer_endpos))

class Token:

    def __init__(self, conllx):
        self.form = conllx.FORM
        self.lemma = conllx.LEMMA
        self.pos = conllx.CPOSTAG
        self.feats = conllx.FEATS
        self.gender = ""
        self.number = ""
        if self.feats:
            for feat in self.feats.split("|"):
                k, v = feat.split("=")
                if k == "g":
                    self.gender = v
                elif k == "n":
                    self.number = v
        self.relation = conllx.DEPREL
        self.startpos = int(conllx.startpos)
        self.endpos = int(conllx.endpos)
        self.outer_startpos = self.startpos
        self.outer_endpos = self.endpos
        self.start_annots = [] # list of starting annotations: (id, refname)
        self.end_annots = [] # list of id ending annotations

    def make_tags(self, inner_text):
        open_tag = '<w talpos=%s lemma=%s function=%s ' \
            'number=%s gender=%s annotstartpos=%s ' \
            'annotendpos=%s>' % (
            quoteattr(self.pos),
            quoteattr(self.lemma),
            quoteattr(self.relation),
            quoteattr(self.number),
            quoteattr(self.gender),
            quoteattr("|".join(str(x[0])+":"+x[1] for x in self.start_annots)),
            quoteattr("|".join(str(x) for x in self.end_annots))
        )
        close_tag = "</w>"
        return open_tag + escape(inner_text) + close_tag

    def absorb_glozz(self, glozz_annots):
        for annot in glozz_annots:
            if annot.start >= self.outer_startpos \
                    and annot.start < self.endpos and not annot.start_is_used:
                assert not annot.start_is_used, str(annot)
                annot.start_is_used = True
                self.start_annots.append((annot.counter, annot.refname))
                #print("start: %s (start = %d, id = %d)" % (annot.refname,
                #    annot.start, annot.counter))
                if not (annot.start >= self.startpos \
                        and annot.start <= self.endpos):
                    print("Warning: Annotation `%d (%d-%d, %s)' outside "
                        "tokens"% (annot.counter, annot.start, annot.end,
                        annot.refname))
            if annot.end >= self.startpos \
                    and annot.end <= self.outer_endpos \
                    and not annot.end_is_used:
                assert not annot.end_is_used, str(annot)
                annot.end_is_used = True
                self.end_annots.append(annot.counter)
                #print("end: %s (start = %d, id = %d)" % (annot.refname,
                #    annot.start, annot.counter))
                if not (annot.end >= self.startpos \
                        and annot.end <= self.endpos):
                    print("Warning: Annotation `%d (%d-%d, %s)' outside "
                        "tokens"% (annot.counter, annot.start, annot.end,
                        annot.refname))

    def __str__(self):
        return make_tags()

########################################################################

class TTToken(Token):

    def __init__(self, form, pos, lemma, startpos, endpos):
        self.form = form
        self.lemma = lemma
        self.pos = pos
        self.startpos = startpos
        self.endpos = endpos
        self.outer_startpos = self.startpos
        self.outer_endpos = self.endpos
        self.start_annots = [] # list of starting annotations: (id, refname)
        self.end_annots = [] # list of id ending annotations

    def make_tags(self, inner_text):
        open_tag = '<w frpos=%s frlemma=%s annotstartpos=%s annotendpos=%s>' \
            % (
            quoteattr(self.pos),
            quoteattr(self.lemma),
            quoteattr("|".join(str(x[0])+":"+x[1] for x in self.start_annots)),
            quoteattr("|".join(str(x) for x in self.end_annots))
        )
        close_tag = "</w>"
        return open_tag + escape(inner_text) + close_tag

########################################################################

def parse_args():
    # definition
    parser = argparse.ArgumentParser(description="Convert Talismane output "
        "to a TEI readable by TXM, possibly including Glozz annotations "
        "(only annotations of type \"MENTION\" are taken into account; the "
        "the referent name must be in a feature called \"REF\").")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--talismane", dest="talismane_file", metavar="FILE",
        help="Talismane output file.")
    group.add_argument("--treetagger", dest="treetagger_file", metavar="FILE",
        help="TreeTagger output file.")
    parser.add_argument("--glozz", dest="glozz_aa_file", metavar="NAME",
        help="Glozz annotation file (.aa).")
    parser.add_argument("--corpus", dest="corpus_file", metavar="NAME",
        help="Corpus file.", required=True)
    parser.add_argument("-o", dest="output_file", metavar="FILE",
        help="Output file (stdout by default).")
    args = parser.parse_args()
    if not (args.talismane_file or args.treetagger_file) \
            and not args.glozz_aa_file:
        parser.print_usage()
        print("error: you must specify a Talismane file or a Glozz file")
    if args.glozz_aa_file and not args.corpus_file.endswith(".ac"):
        parser.print_usage()
        print("error: when you use the --glozz option (.aa file), the "
            "--corpus file must be an .ac file.")
        sys.exit(1)
    for f in (args.glozz_aa_file, args.talismane_file, args.treetagger_file,
            args.corpus_file):
        if f and not os.path.exists(f):
            raise FileNotFoundError(f)
    return args

## TALISMANE ###########################################################

def read_talismane(conll_file, corpus):
    print("Reading CONLL file `%s'..." % conll_file)
    converted_conll = talismaneloc2loc.change_position(
        conll=conll_file, corpus=corpus)
    token_coll = TokenCollection()
    last_end = 0
    for line in converted_conll.split("\n"):
        if line:
            token = Token(ConllXToken(*line.split("\t")))
            token.outer_startpos = last_end
            last_end = token.endpos
            if token_coll.tokens:
                token_coll.tokens[-1].outer_endpos = token.startpos
            token_coll.add(token)
    #print(token_coll.print_positions())
    #input()
    return token_coll

## TREETAGGER ##########################################################

def read_treetagger(tt_file, corpus):
    print("Reading TreeTagger file `%s'..." % tt_file)
    tokens = tt2ttloc.convert(tt_file, corpus=corpus)
    token_coll = TokenCollection()
    last_end = 0
    for t in tokens:
        if t:
            token = TTToken(t.form, t.pos, t.lemma, t.start, t.end)
            token.outer_startpos = last_end
            last_end = token.endpos
            if token_coll.tokens:
                token_coll.tokens[-1].outer_endpos = token.startpos
            token_coll.add(token)
    #print(token_coll.print_positions())
    #input()
    return token_coll


## TEI #################################################################

def make_tei(token_coll, corpus):
    print("Building TEI...")
    # paragraph
    body = token_coll.inject_into_corpus(corpus)
    body = "<p>" + re.sub(r"\n+", "</p><p>", body.strip()) + "</p>"
    # xml
    res = '<TEI version="5.0" xmlns="http://www.tei-c.org/ns/1.0">'
    res += '<teiHeader><fileDesc></fileDesc></teiHeader>'
    res += '<text>'
    res += '<body>'
    res += body
    res += '</body>'
    res += '</text>'
    res += '</TEI>'
    return res

## GLOZZ ###############################################################

def read_glozz(ac_file, aa_file):
    print("Reading Glozz annotation file `%s'..." % ac_file)
    soup = BeautifulSoup(open(aa_file), 'lxml-xml')
    paragraphs = []
    par_counter = 0
    annots = []
    for unit in soup.find_all('unit'):
        kind = str(unit.characterisation.type.string)
        if kind == "paragraph":
            paragraphs.append(GlozzParagraph(
                int(unit.positioning.start.singlePosition['index']),
                int(unit.positioning.end.singlePosition['index']),
                par_counter))
            par_counter += 1
        elif kind == "MENTION":
            refname = ""
            for feature in unit.find_all('feature'):
                if feature['name'] == "REF":
                    refname = str(feature.string)
            annots.append(GlozzAnnotation(
                int(unit.positioning.start.singlePosition['index']),
                int(unit.positioning.end.singlePosition['index']),
                refname))
    # the plain text with paragraphs
    ac = open(ac_file).read()
    paragraphs.sort(key=lambda x: x.start)
    corpus = ""
    for par in paragraphs:
        corpus += ac[par.start:par.end] + "\n\n"
    corpus = corpus.strip()
    #print(corpus)
    # shift the annotations (one par = \n\n = 2 characters)
    for annot in annots:
        for par in paragraphs:
            if annot.start >= par.start and annot.end <= par.end:
                annot.start = annot.start + par.counter*2
                annot.end = annot.end + par.counter*2
                break
    #print(annots)
    return corpus, annots

def inject_glozz_into_talismane(token_coll, glozz_annots):
    print("Injecting glozz annotation into Talismane tokens.")
    token_coll.inject_glozz_annotations(glozz_annots)

## MAIN ################################################################

if __name__ == "__main__":
    args = parse_args()
    res = ""
    if args.glozz_aa_file and args.talismane_file:
        corpus, glozz_annots = read_glozz(args.corpus_file, args.glozz_aa_file)
        corpus_lines = [x+"\n" for x in corpus.split("\n")]
        token_coll = read_talismane(args.talismane_file, corpus_lines)
        inject_glozz_into_talismane(token_coll, glozz_annots)
        res = make_tei(token_coll, corpus)
        #token_coll.print_positions()
        #print("---")
        #for annot in glozz_annots: print(str(annot))
    elif args.glozz_aa_file and args.treetagger_file:
        corpus, glozz_annots = read_glozz(args.corpus_file, args.glozz_aa_file)
        token_coll = read_treetagger(args.treetagger_file, corpus=corpus)
        inject_glozz_into_talismane(token_coll, glozz_annots)
        res = make_tei(token_coll, corpus)
        #token_coll.print_positions()
        #print("---")
        #for annot in glozz_annots: print(str(annot))
    elif args.glozz_aa_file:
        res, glozz_annots = read_glozz(args.corpus_file, args.glozz_aa_file)
    elif args.talismane_file:
        token_coll = read_talismane(args.talismane_file, args.corpus_file)
        corpus = open(args.corpus_file).read()
        res = make_tei(token_coll, corpus)
    elif args.treetagger_file:
        corpus = open(args.corpus_file).read()
        token_coll = read_treetagger(args.treetagger_file, corpus)
        res = make_tei(token_coll, corpus)
    if args.output_file:
        print("Writing `%s'..." % args.output_file)
        open(args.output_file, "w").write(res)
    else:
        print(res)


