# talismane2tei

* **author**: Bruno Oberlé
* **date**: 2017-10-25
* **licence**: GPL v3

## Description

Conversion scripts to help import .txt texts tokenized<sup>[1](#fn1)</sup> and annotated by Talismane and TreeTagger into TXM.

## Documentation

See https://groupes.renater.fr/wiki/txm-users/public/umr_lattice/democrat/importation_talismane_et_glozz

_______
Note

<a name="fn1">1</a>: TXM uses natively its own tokenizer

