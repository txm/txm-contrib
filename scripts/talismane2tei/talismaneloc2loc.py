import collections
import argparse

HELP_TEXT = \
"""Talismane, when used with the --builtInTemplate=with_location option,
outputs a file with a positions issued as "startline startpos endline endpos",
after the ConllX format.  This script convert this position format to the more
traditional "startpos endpos", where startpos and endpos are relative to the
first character, without regards for line numbers.
"""

"""
SYNOPSIS (as module):

# Run Talismane with the --builtInTemplate=with_location option and put the
# output in a file called `conll_file'.  The corpus is in `corpus_file'.
import talismaneloc2loc
converted_conll = talismaneloc2loc.change_position(
    conll="conll_file", corpus="corpus_file")

# or, if you want to write and replace the original file:
talismaneloc2loc.change_position(
    conll="conll_file", corpus="corpus_file", output="conll_file")

"""


ConllXTokenLineLocation = collections.namedtuple('ConllXTokenLineLocation',
    'ID FORM LEMMA CPOSTAG POSTAG FEATS HEAD DEPREL PHEAD PDEPREL ' + \
    'file startline startpos endline endpos')

#ConllXTokenCharLocation = collections.namedtuple('ConllXTokenCharLocation',
#    'ID FORM LEMMA CPOSTAG POSTAG FEATS HEAD DEPREL PHEAD PDEPREL' + \
#    'start end')


def _read_corpus(corpus):
    """ Read the corpus file and return a list of line beginning positions.
    Eg [0, 6, 12] for "apple\npeach\n"""
    lines = []
    char_sum = 0
    if isinstance(corpus, list):
        for line in corpus:
            lines.append(char_sum)
            char_sum += len(line)
    else:
        for line in open(corpus):
            lines.append(char_sum)
            char_sum += len(line)
    return lines

def change_position(conll, corpus, output=None):
    """ Convert the positions from "startline startpos endline endpos" to
    "startpos endpos" (indexed from the file beginning).  Output is an
    optional output filename, which may be the same as conll.  Return the new
    conll data.  """
    line_pos = _read_corpus(corpus)
    res = ""
    for token in open(conll):
        token = token.rstrip()
        if not token:
            res += "\n"
            continue
        token = ConllXTokenLineLocation(*token.split("\t"))
        start = str(line_pos[int(token.startline)-1]-1 + int(token.startpos))
        end = str(line_pos[int(token.endline)-1]-1 + int(token.endpos))
        res += "\t".join(token[0:10] + (start, end))
        res += "\n"
    if output:
        open(output, 'w').write(res)
    return res

########################################################################
# main
########################################################################

def parse_args():
    # definition
    parser = argparse.ArgumentParser(description=HELP_TEXT)
    # arguments (not options)
    parser.add_argument("conll", help="conll file produced by Talismane")
    parser.add_argument("corpus", help="corpus file")
    parser.add_argument("-o", dest="output", default="", metavar="FILE",
        help="output file")
    args = parser.parse_args()
    return args

if __name__ == '__main__':
    args = parse_args()
    res = change_position(conll=args.conll, corpus=args.corpus,
        output=args.output)
    if not args.output:
        print(res)

