name="bouvard_et_pecuchet"
#name="capitaine_fracasse"
#name="diable_au_corps"
#name="douce_lumiere"
#name="nemoville"
#name="ventre_de_paris"

mkdir "/tmp/"$name"_tt"

# pour convertir un texte sans annotation, simplement utiliser la commande
# suivante, où:
#  --corpus est le texte donné à Talismane
#  --talimsane est la sortie de Talismane

#python3 talismane2tei.py \
#   --corpus data/$name \
#   --talismane data/$name".out"

# Attention: il faut utiliser Talismane avec l'option
# --builtInTemplate=with_location, par exemple:
#  java -Xmx4G -Dconfig.file=talismane-fr-5.0.0.conf \
#     -jar talismane-core-5.0.0.jar --analyse \
#     --sessionId=fr --encoding=UTF8 \
#     --builtInTemplate=with_location \
#     --inFile=some_text --outFile=some_text.out

########################################################################

# pour convertir un texte déjà annoté, il faut deux étapes:

# étape 1: à partir des fichiers Glozz .ac et .aa, il faut lancer la commande
# suivante, où:
#  --corpus est le fichier de corpus .ac
#  --glozz est le fichier d'annotations .aa
#  -o est le fichier de sortie, le corpus qu'il faut donner à Talismane (il
#  s'agit du corpus .ac avec les paragraphes séparés par des sauts de ligne).

# python3 talismane2tei.py \
#    --corpus data/$name".ac" \
#    --glozz data/$name".aa" \
#    -o data/$name"_corpus"

# étape 2: Après avoir fait tourner Talismane (avec l'option
# --builtInTemplate=with_location), on emploi la commande suivante, où:
#  --corpus est le fichier de corpus .ac
#  --glozz est le fichier d'annotations .aa
#  --talimsane est la sortie de Talismane
#  -o est le fichier de sortie

#python3 talismane2tei.py \
#   --corpus data/$name".ac" \
#   --talismane data/$name"_corpus.out" \
#   --glozz data/$name".aa" \
#   -o data/$name".tei.xml"

########################################################################

# même chose mais avec treetagger:
python3 talismane2tei.py \
   --corpus data/$name".ac" \
   --treetagger data/$name"_corpus.tt" \
   --glozz data/$name".aa" \
   -o data/$name".tt.tei.xml"



