// @author Bruno Oberle
// 2017-10-21 v1.0.0

package org.txm.macro.analec

import org.apache.commons.lang.*
import org.kohsuke.args4j.*
import groovy.transform.*
import org.txm.*
import org.txm.rcpapplication.swt.widget.parameters.*
import org.txm.analec.*
import org.txm.searchengine.cqp.*
import org.txm.searchengine.cqp.corpus.*
import visuAnalec.Message.*
import visuAnalec.donnees.*
import visuAnalec.elements.*
import visuAnalec.vue.*

// BEGINNING OF PARAMETERS
@Field @Option(name="unitType", usage="", widget="String", required=true, def="MENTION")
def unitType
@Field @Option(name="refnameProperty", usage="", widget="String", required=true, def="REF")
def refnameProperty
@Field @Option(name="clean", usage="", widget="Boolean", required=true, def="true")
def clean
@Field @Option(name="reset", usage="", widget="Boolean", required=true, def="true")
def reset
if (!ParametersDialog.open(this)) return

/** variables **/

MainCorpus corpus = corpusViewSelection
def analecCorpus = AnalecCorpora.getCorpus(corpus.getName());

// structure
def structure = analecCorpus.getStructure()

// CQP
CQI = Toolbox.getCqiClient()
word = corpus.getWordProperty()

startProperty = corpus.getProperty("annotstartpos")
if (!startProperty) {
	println "Error: CQP corpus does not contains the property with name=annotstartpos"
	return
}
endProperty = corpus.getProperty("annotendpos")
if (!endProperty) {
	println "Error: CQP corpus does not contains the property with name=annotendpos"
	return
}


/** structure **/

// check that the structure contains unitType; if not we create it
if (!structure.getUnites().contains(unitType)) {
	println "Adding the unit type $unitType to the structure."
	structure.ajouterType(Unite.class, unitType)
}

// check that unitType contains refnameProperty; if not we add it
if (!structure.getUniteProperties(unitType).contains(refnameProperty)) {
	println "Adding the property $refnameProperty to the unit type $unitType." 
	structure.ajouterProp(Unite.class, unitType, refnameProperty)
}

/** reset (remove all existing annotations) **/

if (reset) {
	println "Removing existing annotations..."
	def units = analecCorpus.getUnites(unitType)
	int counter = 0
	while (units.size()) {
		analecCorpus.supUnite(units.get(0))
		counter++
	}
	println "$counter annotations removed."
}

/** Expression class **/

class Expression {
	int start;
	int end;
	String refname;
	public Expression(int start, int end, String refname) {
		this.start = start;
		this.end = end;
		this.refname = refname;
	}
	public String toString() {
		return this.refname + ": " + this.start.toString() + "--" + this.end.toString()
	}
}

/** looking for the referring expressions **/

HashMap<Integer,Expression> map = new HashMap<Integer,Expression>()

println "Looking for annotations usable by Analec."

size = corpus.getSize() // you may also use: corpus.getTextEndLimits() (= index of last token = size-1)
for (def i=0; i<size; i++) {
	//def word = CQI.cpos2Str(word.getQualifiedName(), (int[])[i])[0]
	def startString = CQI.cpos2Str(startProperty.getQualifiedName(), (int[])[i])[0]
	if (!startString.equals("")) {
		def starts = startString.split("\\|")
		//println starts
		for (String start : starts) {
			String[] split = start.split(":", 2) // id:refname
			//println split
			int id = Integer.valueOf(split[0])
			Expression expr = new Expression(i, i, split[1])
			//println expr.toString()
			map.put(id, expr)
		}
	}
}

for (def i=0; i<size; i++) {
	def endString = CQI.cpos2Str(endProperty.getQualifiedName(), (int[])[i])[0]
	if (!endString.equals("")) {
		def ends = endString.split("\\|")
		for (String end : ends) {
			int id = Integer.valueOf(end)
			Expression expr = map.get(id)
			expr.end = i
		}
	}
}

println map.size() + " annotations found."

/** removing unwanted properties **/

if (clean) {
	println "Cleaning..."
	// FIXME
	corpus.removeProperty("annotstartpos")
	corpus.removeProperty("annotendpos")
}

/** creating the annotations **/

println "Creating the annotations..."

int counter = 0
for (Expression expr : map.values()) {
	//println expr.toString()
	analecCorpus.addUniteSaisie(unitType, expr.start, expr.end)
	def units = analecCorpus.getUnites(unitType) // leave that here (not above, outside the loop!)
	def unit = units.get(units.size()-1)
	unit.getProps().put(refnameProperty, expr.refname)
	counter++
}

println "$counter annotations created."

// udpate the view (also see also
// http://forge.cbp.ens-lyon.fr/redmine/issues/2065)
AnalecCorpora.getVue(analecCorpus).retablirVueParDefaut()