import argparse, sys

# author: Bruno Oberle
# v1.0.0 (2017-10-24)

class Token:
    def __init__(self, form, pos, lemma):
        self.form = form
        self.pos = pos
        self.lemma = lemma
        self.start = 0
        self.end = 0
    def __str__(self):
        return "\t".join((self.form, self.pos, self.lemma, str(self.start),
            str(self.end)))

def _find_pos(corpus, form, current_pos):
    start = corpus.index(form, current_pos)
    end = start + len(form)
    return start, end

def convert(tt_file, *, corpus_file=None, corpus=None):
    """ You must give either corpus_file or corpus. """
    assert corpus_file or corpus
    if corpus_file:
        corpus = open(corpus_file).read()
    tokens = []
    current_pos = 0
    for line in open(tt_file):
        line = line[:-1]
        if line:
            token = Token(*line.split("\t"))
            token.start, token.end = _find_pos(corpus, token.form, current_pos)
            current_pos = token.end
            tokens.append(token)
        else:
            tokens.append(None)
    return tokens

HELP = """TreeTagger has no position indication.  This script outputs a
tab-separated file with such an indication (form pos lemma start end)."""

def parse_args():
    parser = argparse.ArgumentParser(description=HELP)
    parser.add_argument("corpus_file", help="The text used to feed TT.")
    parser.add_argument("treetagger_output", help="The output from TT.")
    parser.add_argument("-o", dest="output_file", required=False,
        help="The output file. If not present, output to stdout.")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = parse_args()
    tokens = convert(tt_file=args.treetagger_output,
        corpus_file=args.corpus_file)
    if args.output_file:
        fh = open(args.output_file, "w")
    else:
        fh = sys.stdout
    for token in tokens:
        if not token:
            fh.write("\n")
        else:
            fh.write(str(token)+"\n")
    if args.output_file:
        fh.close()

